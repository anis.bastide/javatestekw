import java.util.HashMap;

public abstract class Client {
     public String reference;
     public double electricityPrice;
     public double gasPrice;

     public HashMap<String,Double> getInvoice(double electricityConsumption,double gasConsumption){
          double electricityPrice = electricityConsumption * this.electricityPrice;
          double gasPrice = gasConsumption * this.gasPrice;
          HashMap<String,Double> prices = new HashMap<>();
          prices.put("electricity",electricityPrice);
          prices.put("gas",gasPrice);
          return prices;
     }

}
