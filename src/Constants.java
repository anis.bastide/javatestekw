public class Constants {
    public static final double electricityPriceIndividual = 0.121;
    public static final double gasPriceIndividual = 0.115;
    public static final double electricityPriceBigCompany = 0.114;
    public static final double gasPriceBigCompany = 0.111;
    public static final double electricityPriceSmallCompany = 0.118;
    public static final double gasPriceSmallCompany = 0.113;
}
