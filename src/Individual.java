public class Individual extends Client {
        private String title;
        private String name;
        private String surname;


        public Individual(String reference, String title, String name, String surname,double electricityPrice,double gasPrice) {
                this.reference = reference;
                this.title = title;
                this.name = name;
                this.surname = surname;
                this.electricityPrice = electricityPrice;
                this.gasPrice = gasPrice;
        }
}
