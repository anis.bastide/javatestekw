import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Objects;

import static java.lang.Integer.parseInt;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));
        Client client = null;
        boolean isUserInitialized = false;
        while(!isUserInitialized){
        System.out.println("Hello, what is your user type? (Individual,Company)");
        String userType = reader.readLine();
        System.out.println("What is your reference?");
        String reference = reader.readLine();
        while(reference.length() != 11 && !reference.startsWith("EKW")){
            System.out.println("What is your reference? (it starts with EKW and is followed by 8 number");
            reference = reader.readLine();
        }

        if(Objects.equals(userType, "Individual")){
            System.out.println("What is your name?");
            String name = reader.readLine();
            System.out.println("What is your surname?");
            String surname = reader.readLine();
            System.out.println("What is your title");
            String title = reader.readLine();

            client = new Individual(reference,title,name,surname,Constants.electricityPriceIndividual,Constants.gasPriceIndividual);
            isUserInitialized = true;
        }
        else if (Objects.equals(userType, "Company")){
            System.out.println("What is your siret?");
            String siret = reader.readLine();
            while (siret.length()!=14){
                System.out.println("What is your siret? (it's supposed to be 14 numbers)");
                siret = reader.readLine();
            }
            System.out.println("What is your businessName?");
            String businessName = reader.readLine();
            boolean turnoverIsNumber = false;
            int turnover = 0;

            while (!turnoverIsNumber){
                System.out.println("What is your turnover");
                try{
                    turnover = Integer.parseInt(reader.readLine());
                    if(turnover!=0){
                        turnoverIsNumber = true;
                    }
                } catch (NumberFormatException e) {
                    System.out.println("turnover is supposed to be a number");
                }
            }


            client = new Pro(siret,businessName,turnover,reference,turnover > 1000000? Constants.electricityPriceBigCompany : Constants.electricityPriceSmallCompany,turnover > 1000000? Constants.gasPriceBigCompany : Constants.gasPriceSmallCompany);
            isUserInitialized = true;

        }
    }

        System.out.println("How Much did you use your electricity this month? (in Kwh)");
        double electricityQuantity = Double.parseDouble(reader.readLine());
        System.out.println("How Much did you use your gas this month? (in Kwh)");
        double gasQuantity = Double.parseDouble(reader.readLine());
        HashMap<String,Double> prices = client.getInvoice(electricityQuantity,gasQuantity);
        System.out.println("You owe us " + prices.get("electricity") + " euro for electricity and " + prices.get("gas") + " euro for gas.");
    }
}