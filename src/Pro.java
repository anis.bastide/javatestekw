public class Pro extends Client {
    private String siret;
    private String businessName;
    private int turnover;

    public Pro(String siret,String businessName,int turnover,String reference,double electricityPrice,double gasPrice) {
        this.reference = reference;
        this.businessName = businessName;
        this.siret = siret;
        this.turnover = turnover;
        this.electricityPrice = electricityPrice;
        this.gasPrice = gasPrice;
    }
}


